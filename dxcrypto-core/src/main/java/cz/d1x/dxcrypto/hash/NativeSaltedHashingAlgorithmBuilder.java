package cz.d1x.dxcrypto.hash;

import cz.d1x.dxcrypto.common.BytesRepresentation;
import cz.d1x.dxcrypto.common.Encoding;
import cz.d1x.dxcrypto.common.HexRepresentation;

/**
 * Builder for hashing algorithms that have salting built-in by their design (e.g. BCrypt) and this does not need
 * {@link SaltingAdapter}.
 * You should use {@link HashingAlgorithms} factory for creating instances.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 * @see SaltedHashingAlgorithm
 */
public abstract class NativeSaltedHashingAlgorithmBuilder {

    protected BytesRepresentation bytesRepresentation = new HexRepresentation();
    protected String encoding = Encoding.DEFAULT;

    /**
     * Sets how byte arrays will be represented in strings. By default {@link HexRepresentation} is used.
     *
     * @param bytesRepresentation byte array representation strategy
     * @return this instance
     * @throws IllegalArgumentException exception if passed key BytesRepresentation is null
     */
    public NativeSaltedHashingAlgorithmBuilder bytesRepresentation(BytesRepresentation bytesRepresentation) throws IllegalArgumentException {
        if (bytesRepresentation == null) {
            throw new IllegalArgumentException("You must provide non-null BytesRepresentation!");
        }
        this.bytesRepresentation = bytesRepresentation;
        return this;
    }

    /**
     * Sets encoding for strings of input and output.
     *
     * @param encoding encoding to be set
     * @return this instance
     * @throws IllegalArgumentException exception if given encoding is null or not supported
     */
    public NativeSaltedHashingAlgorithmBuilder encoding(String encoding) throws IllegalArgumentException {
        if (encoding == null) {
            throw new IllegalArgumentException("You must provide non-null encoding!");
        }
        Encoding.checkEncoding(encoding);
        this.encoding = encoding;
        return this;
    }

    /**
     * Builds final salted hashing algorithm instance.
     *
     * @return salted hashing algorithm instance
     */
    public abstract SaltedHashingAlgorithm build();

}
