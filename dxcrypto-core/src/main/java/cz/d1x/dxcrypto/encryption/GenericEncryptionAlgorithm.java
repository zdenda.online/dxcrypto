package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.common.ByteArrayFactory;
import cz.d1x.dxcrypto.common.BytesRepresentation;
import cz.d1x.dxcrypto.common.CombiningSplitting;
import cz.d1x.dxcrypto.common.Encoding;
import cz.d1x.dxcrypto.encryption.io.DecryptingOutputStream;
import cz.d1x.dxcrypto.encryption.io.EncryptingInputStream;
import cz.d1x.dxcrypto.encryption.io.IvStreamHelper;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * <p>
 * Main implementation for encryption algorithms that have all logic based on passed {@link EncryptionEngine}.
 * </p><p>
 * The logic is based on whether it is intended for use with or without initialization vector.
 * <ul>
 * <li>Without IV: It simply calls underlying {@link EncryptionEngine} and optionally does encoding/decoding
 * operations.</li>
 * <li>With IV: It generates a new initialization vector from given {@link ByteArrayFactory} and includes in every
 * message based on used {@link CombiningSplitting}. This allows to use one instance for different messages (otherwise
 * it would not be safe to use same combination of key and IV for every message).</li>
 * </ul>
 * <p>
 * This class is immutable and can be considered thread safe. It is not allowed to extend this class to ensure it stays
 * that way.
 * </p>
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public final class GenericEncryptionAlgorithm implements EncryptionAlgorithm {

    private final boolean usesInitVector;
    private final EncryptionEngine engine;
    private final int blockSize;
    private final ByteArrayFactory ivFactory;
    private final CombiningSplitting ivOutputCombining;
    private final BytesRepresentation bytesRepresentation;
    private final String encoding;

    /**
     * Creates a new instance of generic algorithm that uses initialization vector.
     *
     * @param engine              engine for encryption
     * @param bytesRepresentation representation of byte arrays in String
     * @param encoding            encoding used for strings
     * @param blockSize           size of block
     * @param ivFactory           factory used for creation of initialization vector
     * @param ivOutputCombining   algorithm for combining/splitting IV and cipher text
     * @throws EncryptionException exception when algorithm cannot be created
     */
    GenericEncryptionAlgorithm(EncryptionEngine engine,
                               BytesRepresentation bytesRepresentation,
                               String encoding,
                               int blockSize,
                               ByteArrayFactory ivFactory,
                               CombiningSplitting ivOutputCombining) throws EncryptionException {
        this.engine = engine;
        this.bytesRepresentation = bytesRepresentation;
        this.encoding = encoding;

        this.usesInitVector = true;
        this.blockSize = blockSize;
        this.ivFactory = ivFactory;
        this.ivOutputCombining = ivOutputCombining;
    }

    /**
     * Creates a new instance of generic algorithm that does NOT use initialization vector.
     *
     * @param engine              engine for encryption
     * @param bytesRepresentation representation of byte arrays in String
     * @param encoding            encoding used for strings
     * @throws EncryptionException exception when algorithm cannot be created
     */
    protected GenericEncryptionAlgorithm(EncryptionEngine engine,
                                         BytesRepresentation bytesRepresentation,
                                         String encoding) throws EncryptionException {
        this.engine = engine;
        this.bytesRepresentation = bytesRepresentation;
        this.encoding = encoding;

        this.usesInitVector = false;
        this.blockSize = -1;
        this.ivFactory = null;
        this.ivOutputCombining = null;
    }

    @Override
    public byte[] encrypt(byte[] input) throws EncryptionException {
        if (input == null) {
            throw new EncryptionException("Input data for encryption cannot be null!");
        }
        if (usesInitVector) {
            byte[] iv = getIv();
            byte[] encryptedBytes = engine.encrypt(input, iv);
            return ivOutputCombining.combine(iv, encryptedBytes);
        } else {
            return engine.encrypt(input, null);
        }
    }

    @Override
    public String encrypt(String input) throws EncryptionException {
        if (input == null) {
            throw new EncryptionException("Input data for encryption cannot be null!");
        }
        byte[] textBytes = Encoding.getBytes(input, encoding);
        byte[] encryptedBytes = encrypt(textBytes);
        return convertToString(encryptedBytes);
    }

    @Override
    public byte[] decrypt(byte[] input) throws EncryptionException {
        if (input == null) {
            throw new EncryptionException("Input data for decryption cannot be null!");
        }
        if (usesInitVector) {
            byte[][] ivAndCipherText = ivOutputCombining.split(input);
            if (ivAndCipherText == null || ivAndCipherText.length != 2) {
                throw new EncryptionException("Splitting of input into two parts during decryption produced wrong " +
                        "number of parts. Is the input or used implementation of CombiningSplitting correct?");
            }
            return engine.decrypt(ivAndCipherText[1], ivAndCipherText[0]);
        } else {
            return engine.decrypt(input, null);
        }
    }

    @Override
    public String decrypt(String input) throws EncryptionException {
        if (input == null) {
            throw new EncryptionException("Input data for decryption cannot be null!");
        }
        byte[] textBytes = convertToBytes(input);
        byte[] decryptedBytes = decrypt(textBytes);
        return Encoding.getString(decryptedBytes, encoding);
    }

    StreamingEncryptionAlgorithm streaming() {
        return new GenericStreamingEncryptionAlgorithm();
    }

    class GenericStreamingEncryptionAlgorithm implements StreamingEncryptionAlgorithm {
        private final IvStreamHelper ivHelper;
        private final GenericEncryptionAlgorithm genericEncryptionAlgorithm;

        GenericStreamingEncryptionAlgorithm() {
            checkStreamingEngine();
            ivHelper = new IvStreamHelper(blockSize);
            genericEncryptionAlgorithm = GenericEncryptionAlgorithm.this;
        }

        @Override
        public InputStream encrypt(InputStream input) throws EncryptionException {
            final byte[] iv = getIv();
            return new EncryptingInputStream(((StreamingEncryptionEngine) engine).encrypt(input, iv), iv);
        }

        @Override
        public InputStream decrypt(InputStream input) throws EncryptionException {
            final byte[] iv = ivHelper.extractIv(input);
            return ((StreamingEncryptionEngine) engine).decrypt(input, iv);
        }

        @Override
        public OutputStream encrypt(OutputStream output) throws EncryptionException {
            final byte[] iv = getIv();
            ivHelper.writeIv(output, iv);
            return ((StreamingEncryptionEngine) engine).encrypt(output, iv);
        }

        @Override
        public OutputStream decrypt(OutputStream output) throws EncryptionException {
            return new DecryptingOutputStream(output, (StreamingEncryptionEngine) engine, blockSize);
        }

        @Override
        public byte[] encrypt(byte[] input) throws EncryptionException {
            return genericEncryptionAlgorithm.encrypt(input);
        }

        @Override
        public String encrypt(String input) throws EncryptionException {
            return genericEncryptionAlgorithm.encrypt(input);
        }

        @Override
        public byte[] decrypt(byte[] input) throws EncryptionException {
            return genericEncryptionAlgorithm.decrypt(input);
        }

        @Override
        public String decrypt(String input) throws EncryptionException {
            return genericEncryptionAlgorithm.decrypt(input);
        }

        private void checkStreamingEngine() {
            if (!(engine instanceof StreamingEncryptionEngine)) {
                throw new IllegalArgumentException("Stream encryption is supported only by StreamingEncryptionEngine.");
            }
        }

    }

    private byte[] getIv() {
        byte[] ivBytes = ivFactory.getBytes(blockSize);
        if (ivBytes.length != blockSize) {
            throw new EncryptionException("Generated initialization vector has size " + ivBytes.length +
                    " bytes but must be size equal to block size " + blockSize + " bytes");
        }
        return ivBytes;
    }

    private byte[] convertToBytes(String input) {
        try {
            return bytesRepresentation.toBytes(input);
        } catch (IllegalArgumentException e) {
            throw new EncryptionException("Unable to convert input from String to bytes", e);
        }
    }

    private String convertToString(byte[] bytes) {
        try {
            return bytesRepresentation.toString(bytes);
        } catch (IllegalArgumentException e) {
            throw new EncryptionException("Unable to convert output from bytes to String", e);
        }
    }
}
