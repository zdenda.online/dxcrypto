package cz.d1x.dxcrypto.encryption.crypto;

import cz.d1x.dxcrypto.encryption.EncryptionException;
import cz.d1x.dxcrypto.encryption.StreamingEncryptionEngine;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**
 * Implementation of encryption engine that uses javax.crypto implementations for symmetric encryption.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class CryptoSymmetricEngine implements StreamingEncryptionEngine {

    private final String cipherName;
    private final Key key;

    public CryptoSymmetricEngine(String cipherName, byte[] key) throws EncryptionException {
        this.cipherName = cipherName;
        String shortCipherName = cipherName.contains("/") ? cipherName.substring(0, cipherName.indexOf("/")) : cipherName;
        checkJCE(shortCipherName, key.length); // Crypto may require JCE installed
        this.key = new SecretKeySpec(key, shortCipherName);
        try {
            Cipher.getInstance(cipherName); // find out if i can create instances and retrieve block size
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new EncryptionException("Invalid encryption algorithm", e);
        }
    }

    @Override
    public byte[] encrypt(byte[] input, byte[] initVector) throws EncryptionException {
        return doOperation(input, initVector, true);
    }

    @Override
    public OutputStream encrypt(OutputStream output, byte[] initVector) {
        final Cipher cipher = createCipher(initVector, true);
        return new CipherOutputStream(output, cipher);
    }

    @Override
    public OutputStream decrypt(OutputStream output, byte[] initVector) throws EncryptionException {
        final Cipher cipher = createCipher(initVector, false);
        return new CipherOutputStream(output, cipher);
    }

    @Override
    public byte[] decrypt(byte[] input, byte[] initVector) {
        return doOperation(input, initVector, false);
    }

    @Override
    public InputStream encrypt(InputStream input, byte[] initVector) throws EncryptionException {
        final Cipher cipher = createCipher(initVector, true);
        return new CipherInputStream(input, cipher);
    }

    @Override
    public InputStream decrypt(InputStream input, byte[] initVector) {
        final Cipher cipher = createCipher(initVector, false);
        return new CipherInputStream(input, cipher);
    }

    private Cipher createCipher(byte[] initVector, boolean isEncrypt) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector);
            Cipher cipher = Cipher.getInstance(cipherName);
            cipher.init(isEncrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, key, iv);
            return cipher;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException e) {
            throw new EncryptionException("Unable to create cipher", e);
        }
    }

    private byte[] doOperation(byte[] input, byte[] initVector, boolean isEncrypt) {
        try {
            Cipher cipher = createCipher(initVector, isEncrypt);
            return cipher.doFinal(input);
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            throw new EncryptionException("Unable to encrypt input", e);
        }
    }

    private void checkJCE(String name, int keySize) {
        IllegalArgumentException exc = new IllegalArgumentException("Cipher " + name + " is not supported with key " +
                "size of " + keySize + "b,  probably Java Cryptography Extension (JCE) is not installed in your Java.");
        try {
            if (Cipher.getMaxAllowedKeyLength(name) < keySize) {
                throw exc;
            }
        } catch (NoSuchAlgorithmException e) {
            throw exc;
        }
    }
}
