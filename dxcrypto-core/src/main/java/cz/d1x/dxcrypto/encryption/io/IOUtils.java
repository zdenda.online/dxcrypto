package cz.d1x.dxcrypto.encryption.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * Utils class for working with IO
 *
 * @author d.richter
 */
public class IOUtils {
    private static final int EOF = -1;

    public static int read(InputStream input, byte[] buffer) throws IOException {
        int remaining = buffer.length;
        while (remaining > 0) {
            final int location = buffer.length - remaining;
            final int count = input.read(buffer, location, remaining);
            if (EOF == count) { // EOF
                break;
            }
            remaining -= count;
        }
        return buffer.length - remaining;
    }
}
