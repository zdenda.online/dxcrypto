package cz.d1x.dxcrypto.encryption;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface for encryption engines that adds possibility to encrypt and decrypt streams.
 * Note that implementations should be immutable (should not change its internal state by
 * any of the methods).
 *
 * @author d.richter
 */
public interface StreamingEncryptionEngine extends EncryptionEngine {

    /**
     * Encrypts given {@link InputStream} and using given initialization vector (if needed).
     *
     * @param input      InputStream to be encrypted
     * @param initVector initialization vector for encryption if needed (can be null)
     * @return encryption InputStream
     * @throws EncryptionException exception if encryption fails
     */
    InputStream encrypt(InputStream input, byte[] initVector) throws EncryptionException;

    /**
     * Encrypts given {@link OutputStream} and using given initialization vector (if needed).
     *
     * @param output     OutputStream to be encrypted
     * @param initVector initialization vector for encryption if needed (can be null)
     * @return encryption OutputStream
     * @throws EncryptionException exception if encryption fails
     */
    OutputStream encrypt(OutputStream output, byte[] initVector) throws EncryptionException;

    /**
     * Decrypts given {@link InputStream} and using given initialization vector (if needed).
     *
     * @param input      InputStream to be decrypted
     * @param initVector initialization vector for decryption if needed (can be null)
     * @return decryption InputStream
     * @throws EncryptionException exception if decryption fails
     */
    InputStream decrypt(InputStream input, byte[] initVector) throws EncryptionException;

    /**
     * Decrypts given {@link OutputStream} and using given initialization vector (if needed).
     *
     * @param output     OutputStream to be decrypted
     * @param initVector initialization vector for decryption if needed (can be null)
     * @return decryption OutputStream
     * @throws EncryptionException exception if decryption fails
     */
    OutputStream decrypt(OutputStream output, byte[] initVector) throws EncryptionException;
}
