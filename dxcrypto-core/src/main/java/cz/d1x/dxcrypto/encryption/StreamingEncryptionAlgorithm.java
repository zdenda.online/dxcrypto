package cz.d1x.dxcrypto.encryption;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface thats adds encrypt and decrypt streams ability to {@link EncryptionAlgorithm} interface.
 *
 * @author d.richter
 * @see EncryptionAlgorithm
 */
public interface StreamingEncryptionAlgorithm extends EncryptionAlgorithm {

    /**
     * Encrypts specified {@link InputStream}.
     *
     * @param input InputStream to be encrypted
     * @return encrypting stream
     * @throws EncryptionException possible exception when encryption fails
     */
    InputStream encrypt(InputStream input) throws EncryptionException;

    /**
     * Decrypts specified {@link InputStream}.
     *
     * @param input InputStream to be decrypted
     * @return decrypting stream
     * @throws EncryptionException possible exception when decryption fails
     */
    InputStream decrypt(InputStream input) throws EncryptionException;

    /**
     * Encrypts specified {@link OutputStream}.
     *
     * @param output OutputStream to be encrypted
     * @return encrypting stream
     * @throws EncryptionException possible exception when encryption fails
     */
    OutputStream encrypt(OutputStream output) throws EncryptionException;

    /**
     * Decrypts specified {@link OutputStream}.
     *
     * @param output OutputStream to be decrypted
     * @return decrypting stream
     * @throws EncryptionException possible exception when decryption fails
     */
    OutputStream decrypt(OutputStream output) throws EncryptionException;
}
