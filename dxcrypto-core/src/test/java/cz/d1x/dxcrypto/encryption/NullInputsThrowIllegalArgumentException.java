package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.TestResult;

/**
 * Tests that invalid input fail appropriately.
 */
public class NullInputsThrowIllegalArgumentException implements EncryptionTest {

    @Override
    public TestResult test(EncryptionAlgorithm algorithm) {
        try {
            algorithm.encrypt((byte[]) null);
            return TestResult.fail("Expected IllegalArgumentException");
        } catch (EncryptionException e) {
            // this is OK
        }
        try {
            algorithm.encrypt((String) null);
            return TestResult.fail("Expected IllegalArgumentException");
        } catch (EncryptionException e) {
            // this is OK
        }
        try {
            algorithm.decrypt((byte[]) null);
            return TestResult.fail("Expected IllegalArgumentException");
        } catch (EncryptionException e) {
            // this is OK
        }
        try {
            algorithm.decrypt((String) null);
            return TestResult.fail("Expected IllegalArgumentException");
        } catch (EncryptionException e) {
            // this is OK
        }

        return TestResult.success();
    }
}
