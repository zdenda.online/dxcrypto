package cz.d1x.dxcrypto.encryption.io;

import cz.d1x.dxcrypto.encryption.StreamingEncryptionEngine;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Test for {@link DecryptingOutputStream}
 * tests correct initialization of cipher by reading first bytes as IV.
 *
 * @author d.richter
 */
public class DecryptingOutputStreamTest {

    @Test
    public void writesUpToBlockSizeAreNotWrittenToOutput() throws IOException {
        final StreamingEncryptionEngine engineMock = mock(StreamingEncryptionEngine.class);
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        when(engineMock.decrypt(any(OutputStream.class), any(byte[].class))).thenReturn(output);

        final DecryptingOutputStream decryptingOutputStream = new DecryptingOutputStream(output, engineMock, 16);

        final byte[] iv = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        decryptingOutputStream.write(iv);
        verify(engineMock).decrypt(output, iv);
        assertArrayEquals("Output was written before whole IV was read.", new byte[0], output.toByteArray());
    }

    @Test
    public void writesAfterBlockSizeAreWrittenToOutput() throws IOException {
        final StreamingEncryptionEngine engineMock = mock(StreamingEncryptionEngine.class);
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        when(engineMock.decrypt(any(OutputStream.class), any(byte[].class))).thenReturn(output);

        final DecryptingOutputStream decryptingOutputStream = new DecryptingOutputStream(output, engineMock, 16);

        decryptingOutputStream.write(new byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20});
        assertArrayEquals("Wrong bytes was written to output.", new byte[] {16, 17, 18, 19, 20}, output.toByteArray());
    }
}