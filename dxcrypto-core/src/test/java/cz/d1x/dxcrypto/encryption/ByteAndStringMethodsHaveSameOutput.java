package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.TestResult;
import cz.d1x.dxcrypto.common.HexConverter;

/**
 * Tests that byte and string encryption methods have same output.
 */
public class ByteAndStringMethodsHaveSameOutput implements EncryptionTest {

    @Override
    public TestResult test(EncryptionAlgorithm algorithm) {
        String plainString = "Som3 T@";
        byte[] plainBytes = new byte[]{'S', 'o', 'm', '3', ' ', 'T', '@'};
        byte[] encrypted = algorithm.encrypt(plainBytes);
        String decrypted = algorithm.decrypt(HexConverter.printHexBinary(encrypted).toLowerCase());
        if (!plainString.equals(decrypted)) {
            return TestResult.fail("Original and decrypted strings are not equal");
        } else {
            return TestResult.success();
        }
    }
}
