package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.encryption.key.RSAKeyParams;
import org.junit.Assert;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Tests {@link EncryptionAlgorithm} implementations.
 * Note that it tests only basic scenarios with default encoding (as it calls other encrypt/decrypt methods).
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class EncryptionAlgorithmsTest {

    private static final byte[] AES_KEY = {
            0x27, 0x18, 0x27, 0x09, 0x7C, 0x44, 0x17, 0x1E,
            0x43, 0x03, 0x11, 0x27, 0x1F, 0x0D, 0x6D, 0x64};
    private static final byte[] TRIPLE_DES_KEY = {
            0x27, 0x18, 0x27, 0x09, 0x7C, 0x44, 0x17, 0x1E,
            0x43, 0x03, 0x11, 0x27, 0x1F, 0x0D, 0x6D, 0x64,
            0x44, 0x18, 0x27, 0x09, 0x7A, 0x44, 0x17, 0x3E};
    private static final RSAKeyParams[] RSA_KEYS = new RSAKeysGenerator().generateKeys();

    protected List<EncryptionAlgorithm> getImplementationsToTest() {
        return new ArrayList<EncryptionAlgorithm>() {{
            add(EncryptionAlgorithms.aes(AES_KEY).build());
            add(EncryptionAlgorithms.tripleDes(TRIPLE_DES_KEY).build());

            add(EncryptionAlgorithms.rsa()
                    .publicKey(RSA_KEYS[0].getModulus(), RSA_KEYS[0].getExponent())
                    .privateKey(RSA_KEYS[1].getModulus(), RSA_KEYS[1].getExponent())
                    .build());
        }};
    }


    /**
     * Tests whether encryption and decryption works if different instances with same key are used for each operation.
     */
    @Test
    public void differentInstanceWithSameKeyGiveSameResults() throws UnsupportedEncodingException {
        List<EncryptionAlgorithm> encryptionAlgorithms1 = getImplementationsToTest();
        List<EncryptionAlgorithm> encryptionAlgorithms2 = getImplementationsToTest();

        String plain = "th1s_is_something inter3sting -*";
        for (int i = 0; i < encryptionAlgorithms1.size(); i++) {
            EncryptionAlgorithm alg1 = encryptionAlgorithms1.get(i);
            EncryptionAlgorithm alg2 = encryptionAlgorithms2.get(i);

            byte[] plainBytes = plain.getBytes("UTF-8");
            byte[] encryptedBytes = alg1.encrypt(plainBytes);
            byte[] decryptedBytes = alg2.decrypt(encryptedBytes); // using different instance but same key for decrypt
            Assert.assertArrayEquals("Original and decrypted strings are not equal", plainBytes, decryptedBytes);
        }
    }

}
