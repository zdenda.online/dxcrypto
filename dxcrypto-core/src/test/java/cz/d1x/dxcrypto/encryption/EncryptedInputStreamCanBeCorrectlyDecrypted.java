package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.TestResult;
import cz.d1x.dxcrypto.encryption.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Simple test that encryption and decryption of stream works correctly
 */
public class EncryptedInputStreamCanBeCorrectlyDecrypted implements EncryptionTest {

    @Override
    public TestResult test(EncryptionAlgorithm algorithm) {
        String plainString = "Som3 T@";
        if (!(algorithm instanceof StreamingEncryptionAlgorithm)) {
            return TestResult.success();
        }

        final StreamingEncryptionAlgorithm streamingAlgorithm = (StreamingEncryptionAlgorithm) algorithm;

        final ByteArrayInputStream input = new ByteArrayInputStream(plainString.getBytes(UTF_8));
        final byte[] encryptedBytes;
        try (InputStream encrypted = streamingAlgorithm.encrypt(input)) {
            encryptedBytes = readWholeInputStream(encrypted);
        } catch (IOException e) {
            e.printStackTrace();
            return TestResult.fail(e.getMessage());
        }
        if (new String(encryptedBytes, UTF_8).contains(plainString)) {
            return TestResult.fail("Encrypted string contains original string");
        }

        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        try (OutputStream decrypted = streamingAlgorithm.decrypt(output)) {
            decrypted.write(encryptedBytes);
        } catch (IOException e) {
            e.printStackTrace();
            return TestResult.fail(e.getMessage());
        }
        final String decryptedString;
        try (InputStream decrypted = streamingAlgorithm.decrypt(new ByteArrayInputStream(encryptedBytes))) {
            final byte[] decryptedBytes = readWholeInputStream(decrypted);
            decryptedString = new String(decryptedBytes, UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return TestResult.fail(e.getMessage());
        }
        if (!new String(output.toByteArray(), UTF_8).equals(plainString)) {
            return TestResult.fail("Original and decrypted strings does not match");
        }
        if (!decryptedString.equals(plainString)) {
            return TestResult.fail("Original and decrypted strings does not match");
        }
        return TestResult.success();
    }

    private byte[] readWholeInputStream(InputStream decrypted) throws IOException {
        byte[] buffer = new byte[1024];
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        int read;
        do {
            read = IOUtils.read(decrypted, buffer);
            result.write(buffer, 0, read);
        } while (read > 0);
        return result.toByteArray();
    }
}
