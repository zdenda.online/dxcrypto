package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.TestResult;
import org.junit.Assert;

import java.util.Collection;

/**
 * Base class that contains encryption tests for various implementations.
 * For testing a specific implementation, you can override any method from this base class and annotate wrapping
 * method by junit test annotation.
 */
public abstract class BaseEncryptionTests {

    protected abstract Collection<EncryptionAlgorithm> getAlgorithmsToTest();

    public void nullInputsThrowEncryptionException() {
        doTest(new NullInputsThrowIllegalArgumentException());
    }

    public void encryptedInputCanBeCorrectlyDecrypted() {
        doTest(new EncryptedInputCanBeCorrectlyDecrypted());
    }

    public void byteAndStringBasedMethodsGiveSameOutput() {
        doTest(new ByteAndStringMethodsHaveSameOutput());
    }

    public void sameInputsHaveDifferentOutputsForSymmetricAlgorithms() {
        doTest(new SameInputsHaveDifferentOutputForSymmetric());
    }

    public void encryptedInputStreamCanBeCorrectlyDecrypted() {
        doTest(new EncryptedInputStreamCanBeCorrectlyDecrypted());
    }

    public void encryptedOutputStreamCanBeCorrectlyDecrypted() {
        doTest(new EncryptedOutputStreamCanBeCorrectlyDecrypted());
    }

    public void concurrentEncryptingThreadsDoesNotInterfere() {
        doTest(new ConcurrencyEncryptionTest());
    }


    private void doTest(EncryptionTest test) {
        for (EncryptionAlgorithm algorithm : getAlgorithmsToTest()) {
            TestResult result = test.test(algorithm);
            if (result.isFailed()) Assert.fail(result.getErrorMessage());
        }
    }
}
