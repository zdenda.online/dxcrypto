package cz.d1x.dxcrypto.encryption.io;

import cz.d1x.dxcrypto.encryption.EncryptionException;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Tests {@link IvStreamHelper}
 * Verifies correct behaviour of extractIv and writeIv methods
 *
 * @author d.richter
 */
public class IvStreamHelperTest {

    private final static byte[] LONG_INPUT_BYTES = new byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    private final byte[] SHORT_INPUT_BYTES = new byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    @Test
    public void extractIvReturnsCorrectIv() {
        final IvStreamHelper helper = new IvStreamHelper(16);
        final ByteArrayInputStream input = new ByteArrayInputStream(LONG_INPUT_BYTES);
        final byte[] extractedIv = helper.extractIv(input);

        final byte[] expectedIv = new byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        Assert.assertArrayEquals(expectedIv, extractedIv);
    }

    @Test
    public void extractIvLeavesRestOfTheStreamUntouched() throws IOException {
        final IvStreamHelper helper = new IvStreamHelper(16);
        final ByteArrayInputStream input = new ByteArrayInputStream(LONG_INPUT_BYTES);
        helper.extractIv(input);

        final byte[] restOfStreamBuffer = new byte[5];
        Assert.assertEquals(5, input.read(restOfStreamBuffer));
        Assert.assertEquals(-1, input.read());
        Assert.assertArrayEquals(new byte[] {16, 17, 18, 19, 20}, restOfStreamBuffer);
    }

    @Test(expected = EncryptionException.class)
    public void exceptionIsThrownIfStreamIsShorterThanBlockSize() {
        final IvStreamHelper helper = new IvStreamHelper(16);
        final ByteArrayInputStream input = new ByteArrayInputStream(SHORT_INPUT_BYTES);
        helper.extractIv(input);
    }

    @Test(expected = EncryptionException.class)
    public void whenReadThrowsExceptionEncryptionExceptionIsThrown() {
        final IvStreamHelper helper = new IvStreamHelper(16);
        final InputStream input = new InputStream() {
            @Override
            public synchronized int read() throws IOException {
                throw new IOException("You shall not read from this stream.");
            }
        };
        helper.extractIv(input);
    }

    @Test
    public void ivIsWrittenCorrectly() {
        final IvStreamHelper helper = new IvStreamHelper(16);
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        helper.writeIv(output, SHORT_INPUT_BYTES);
        Assert.assertArrayEquals(SHORT_INPUT_BYTES, output.toByteArray());
    }

    @Test(expected = EncryptionException.class)
    public void whenWriteThrowsExceptionEncryptionExceptionIsThrown() {
        final IvStreamHelper helper = new IvStreamHelper(16);
        final OutputStream output = new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                throw new IOException("You shall not write to this stream.");
            }
        };
        helper.writeIv(output, SHORT_INPUT_BYTES);
    }
}