package cz.d1x.dxcrypto.hash.bc;

import cz.d1x.dxcrypto.hash.HashingAlgorithm;
import cz.d1x.dxcrypto.hash.NativeSaltedHashingAlgorithmBuilder;
import cz.d1x.dxcrypto.hash.SaltedHashingAlgorithm;

/**
 * Factory that provides builders for available Bouncy Castle hashing algorithms.
 * Create a new builder and when you are done with parameters, call build.
 * to retrieve immutable {@link HashingAlgorithm} or {@link SaltedHashingAlgorithm} instance.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class BouncyCastleHashingAlgorithms {

    /**
     * Creates a new builder for BCrypt salted hashing algorithm.
     *
     * @return builder for MD5
     */
    public static NativeSaltedHashingAlgorithmBuilder bcrypt() {
        return new BouncyCastleBcryptBuilder();
    }
}
